#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#ifdef CPU
#define __device__
#define __host__
#else
#include <cuda.h>
#endif

// combinatorics :)
// TODO: recursion; factor out if using in loop
int binom(int m, int n)
{
	if (n > m) return 0;
	if (n == m) return 1;
	if (n == 0) return 1;
	return binom(m-1, n-1) + binom(m-1, n);
}

void assert_real(const float *x, int n)
{
	for (int i=0; i<n; i++) {
		if (x[i] != x[i]) printf("i = %d\n", i);
		assert(x[i]==x[i]);
		assert(x[i]+1e38!=x[i]);
	}
}

void print_matrix(float *A, int m, int n)
{
	for (int i=0; i<m; i++) {
		for (int j=0; j<n; j++) {
			printf("% 12.4g ", A[j*m + i]);
		}
		printf("\n");
	}
}

// C++ is mean
extern "C" {
	enum dictionary { // dictionary
		D_POLY=0, // Laguerre polynomials
		D_DSE=1, // discontinuous spectral elements
	};
	enum pinv { // method of pseudoinversion
		PI_RRQR, // QR with column pivoting
	};
}

// Configuration
struct config {
	enum dictionary dict;
	union { // dictionary options
		struct {
			int order; // order of polynomials
		} poly;
		struct {
			int order; // order of polynomials
			int box_size; // maximum number of box points
			float *box_min; // minimum components of phase space
			float *box_max; // maximum components of phase space
		} dse;
	} dict_opts;
	int N; // number of datapoints
	int n; // dimension of data
	int m; // dimension of dictionary
	int p; // number of Koopman eigenvalues to approximate
	float tol; // tolerance for Koopman eigenvalues (assumes exact arithmatic)
	char *infile; // file to read data from
	char *outfile; // file to write data to
	enum pinv pinv;
	char *eigencsv; // file to write eigenvalues to or NULL
} hg_config;

// box tree datastructure
struct box {
	int blk; // box for block uprposes
	int dim; // dimension to split on, negative for do not split
	float cutoff; // value to split on
	int low, high; // index for if below or above cutoff
};

struct box *h_boxes; // host array of boxes
int hg_box_tree_size; // number of boxes allocated
int hg_box_count = 1; // number of boxes in h_boxes
int hg_box_back; // backing size of h_boxes
// h_blk_idx is hg_box_count x hg_box_count, while each block is m x m
int *h_blk_idx; // index of block in sparse matrices
int hg_box_adjs; // number of box adjanancies (max h_blk_idx)
// h_idx_blk is 2 x bg_box_adjs
int *h_idx_blk; // block of index in sparse matrices, 0=row, 1=col

// split this box
// TODO: move cutoff if data unbalanced
void make_boxes_helper(const float *data, int box, const float *start, const float *stop, int size, int depth)
{
//	printf("box %d has %d points\n", box, size);
//	printf("line 69: %f\n", data[0]);
	int n = hg_config.n;
	int counts[n]; // number of datapoints on low side of cutoff
	for (int i=0; i<n; i++) {
		counts[i] = 0;
	}
	for (int i=0; i<hg_config.N; i++) {
		for (int j=0; j<hg_config.n; j++) {
//			printf("i:%d j:%d ", i, j);
//			printf("d:%f ", data[n*i + j]);
//			printf("a:%f ", start[j]);
//			printf("o:%f\n", stop[j]);
			if (data[n*i + j] < start[j] || data[n*i + j] > stop[j]) {
				continue; // not in box
			} else if (data[n*i + j] < (start[j]+stop[j])/2) {
				counts[j]++;
			}
		}
	}
	int min_dth = size*2; // minimum count distance to half of size
	int min_dim = -1; // dimension of min_dth
	for (int j=0; j<hg_config.n; j++) {
		int dth = abs(counts[j] - size/2);
		if (dth < min_dth) {
			min_dth = dth;
			min_dim = j;
		}
	}
	float cutoff = (start[min_dim]+stop[min_dim])/2;

	if (hg_box_tree_size+2 >= hg_box_back) {
		hg_box_back *= 2;
		h_boxes = (struct box*) realloc(h_boxes, hg_box_back*sizeof(struct box));
	}
	if (depth > 100) {
		return;
	}
	float *tmp = (float*) malloc(sizeof(float) * hg_config.n);
	if (counts[min_dim] > hg_config.dict_opts.dse.box_size && counts[min_dim] < size
			|| size-counts[min_dim] > hg_config.dict_opts.dse.box_size && counts[min_dim] > 0) {
		h_boxes[box].dim = min_dim;
		h_boxes[box].cutoff = cutoff;
//		printf("line 111 bc = %d\n", hg_box_count);
		h_boxes[box].low = hg_box_tree_size++;
		h_boxes[box].high = hg_box_tree_size++;
//		printf("line 114 bc = %d\n", hg_box_count);
		h_boxes[h_boxes[box].low].dim = -1;
		h_boxes[h_boxes[box].high].dim = -1;
		h_boxes[h_boxes[box].low].blk = h_boxes[box].blk;
		h_boxes[h_boxes[box].high].blk = hg_box_count++;
		for (int j=0; j<hg_config.n; j++) {
			tmp[j] = stop[j];
		}
		tmp[min_dim] = cutoff;
		make_boxes_helper(data, h_boxes[box].low, start, tmp, counts[min_dim], depth+1);
		for (int j=0; j<hg_config.n; j++) {
			tmp[j] = start[j];
		}
		tmp[min_dim] = cutoff;
		make_boxes_helper(data, h_boxes[box].high, tmp, stop, size-counts[min_dim], depth+1);
	}
	free(tmp);
//	printf("box %d is block %d\n", box, h_boxes[box].blk);
}

void make_box_graph(const float *data, int N, int n)
{
//	printf("box_count = %d\n", hg_box_count);
	h_blk_idx = (int*) malloc(hg_box_count*hg_box_count   * sizeof(int));
	h_idx_blk = (int*) malloc(hg_box_count*hg_box_count*2 * sizeof(int));
	for (int i=0; i < hg_box_count*hg_box_count; i++) {
		h_blk_idx[i] = -1;
	}

	int box1, box2; // current and next box
	for (int i=0; i<N; i++) {
		int box = 0;
//		printf("boxes[0].dim=%d\n", h_boxes[0].dim);
		while (h_boxes[box].dim >= 0) {
			if (data[i*n + h_boxes[box].dim] < h_boxes[box].cutoff) {
//				printf("Taking low of %d to %d\n", box, h_boxes[box].low);
				box = h_boxes[box].low;
			} else {
//				printf("Taking high of %d to %d\n", box, h_boxes[box].high);
				box = h_boxes[box].high;
			}
		}
//		assert(box > 0);
		int blk = h_boxes[box].blk;
//		printf("box %d has block %d\n", box, blk);
		if (i > 0 && h_blk_idx[box2*hg_box_count + blk] == -1) {
//			printf("ba=%d bc=%d b1=%d b2=%d bc2=%d\n", hg_box_adjs, hg_box_count, box1, box2, hg_box_count*hg_box_count);
			box1 = box2;
			h_idx_blk[hg_box_adjs*2 + 0] = box1;
			h_idx_blk[hg_box_adjs*2 + 1] = blk;
			h_blk_idx[box1*hg_box_count + blk] = hg_box_adjs++;
		}
		box2 = blk;
	}
	h_idx_blk = (int*) realloc(h_idx_blk, hg_box_adjs*2 * sizeof(int));
}

void make_boxes(const float *data)
{
	hg_box_back = 32;
	hg_box_count = 1;
	h_boxes = (struct box*) malloc(hg_box_back*sizeof(struct box));
	h_boxes[0].dim = -1;
	h_boxes[0].blk = 0;
	make_boxes_helper(data, hg_box_tree_size++,
			hg_config.dict_opts.dse.box_min, hg_config.dict_opts.dse.box_max,
			hg_config.N, 0);
	make_box_graph(data, hg_config.N, hg_config.n);
}

void setup_dict(const float *data)
{
	switch (hg_config.dict) {
	case D_POLY:
		hg_config.m = binom(hg_config.n+hg_config.dict_opts.poly.order, hg_config.n);
		break;
	case D_DSE:
		make_boxes(data);
		hg_config.m = binom(hg_config.n+hg_config.dict_opts.dse.order, hg_config.n);
		printf("%d boxes, %d adjanencies\n", hg_box_count, hg_box_adjs);
		break;
	default:
		fprintf(stderr, "Unrecognised dictionary %d\n", hg_config.dict);
		exit(1);
	}
}

#define DMAX 8

// evaluate multivariate (n vars) basis (up to degree dmax) of Laguerre polynomials
// assumes dmax < n and that dmax is small (e.g. 4)
// ordered by number of included variables (ascending) then lexicographic
// does no synchronisation on GPU
__device__ __host__ void laguerre(int dmax, int n, const float *t, float *out)
{
	// edge case
	out[0] = 1;

	// setup
	int active[DMAX]; // whan variables are we using
	int d[DMAX]; // degrees along variables
	float ld[DMAX], ld1[DMAX]; // Laguerre evaluations of order d and d-1
	float x[DMAX]; // cached t along active matrix

	// main loop; write "monomials"
	int oi = 1; // output index
	for (int nv=1; nv<=dmax; nv++) { // loop over nuber of variables
		for (int i=0; i<nv; i++) {
			active[i] = i;
		}
		while (true) { // loop over active sets
			// setup
			int da = nv; // total degree of multivariate "monomial"
			for (int i=0; i<nv; i++) {
				x[i] = t[active[i]];
				ld1[i] = 1;
				ld[i] = x[i];
				d[i] = 1;
			}
			// loop over monomial configurations
			while (true) {
				// write monomial
				out[oi] = 1;
				for (int j=0; j<nv; j++) {
					out[oi] *= ld[j];
//					printf("x%d^%d ", active[j], d[j]);
				}
//				printf("\n");
				oi++;
				// evaluate next univariate monomials
				int i;
				for (i=0; i<nv && (d[i]++) > dmax-da; i++) {
//					printf("d[%d] = %d, nv = %d, da=%d\n", i, d[i], nv, da);
					ld1[i] = 1;
					ld[i] = x[i];
					da += 2 - d[i];
					assert(da >= nv);
					d[i] = 1;
				}
				da++;
				if (i == nv) break;
				float ldn = ((2*d[i]-x[i])*ld[i] - (d[i]-1)*ld1[i]) / d[i];
				ld1[i] = ld[i];
				ld[i] = ldn;
			}
			// advance to next active set
			if (active[nv-1] == n-1) {
				int i;
				for (i=nv-2; i >= 0 && active[i]+1 == active[i+1]; i--);
				if (i < 0) break; // enumeration of active sets finished
				active[i]++;
				for (int j=i+1; j<nv; j++) {
					active[j] = active[i] + j - i;
				}
			} else {
				active[nv-1]++;
			}
		}
	}
}

// evaluate dse at one point
// pass h_boxes or c_boxes in boxes, hg_config.n in n
// pass hg_config.dict_opts.dse.box_{min,max} in box_{min,max}
// needs n dimensional work vectors work0, work1, work2
__device__ __host__ void eval_dse(const float *data, float *out, int *out_box,
		int n, const struct box *boxes, const float *box_min, const float *box_max,
		int order, float *work0, float *work1, float *work2)
{
	float *start = work0;
	float *len = work1;
	for (int j=0; j<n; j++) {
		start[j] = box_min[j];
		len[j] = box_max[j] - start[j];
	}

	int box = 0;
	while (boxes[box].dim >= 0) {
		len[boxes[box].dim] = len[boxes[box].dim] / 2;
		if (data[boxes[box].dim] < boxes[box].cutoff) {
			box = boxes[box].low;
		} else {
			start[boxes[box].dim] = boxes[box].cutoff;
			box = boxes[box].high;
		}
	}

	float *t = work2;
	for (int j=0; j<n; j++) {
		t[j] = (data[j]-start[j]) / len[j];
	}

	laguerre(order, n, t, out);
	*out_box = boxes[box].blk;
}

// HOST VERSION
// form G and A for dse (times N since the constants cancel)
// G is symmetric and block diagonal, so we store it as
// (essentially) an array of m x m upper triangular matrices
// corresponding to the diagonals in order from top left
// A isn't as nice, but it is in sparse blocks, corresponding to boxes
// when picking the boxes, we found the number of unique
// box adjacencies, which determines the backing size of A
// the specific box adjanancy taken by a given block of A
// is specified by looking up the entries in h_idx_blk
void formga_dse(const float *data, float *G, float *A)
{
	// for convinience
	int n = hg_config.n;
	int m = hg_config.m;
	int bc = hg_box_count;

	float *x1 = (float*) calloc(m, sizeof(float)); // Phi(xm)
	float *x2 = (float*) calloc(m, sizeof(float)); // Phi(ym)
	float work0[n], work1[n], work2[n];
	int box1, box2; // box x1 and x2 are in

	memset(G, 0, m*m*bc*sizeof(float));
	memset(A, 0, m*m*hg_box_adjs*sizeof(float));

	eval_dse(data, x2, &box2, n, h_boxes,
			hg_config.dict_opts.dse.box_min, hg_config.dict_opts.dse.box_max,
			hg_config.dict_opts.dse.order, work0, work1, work2);
	for (int i=0; i<hg_config.N-1; i++) {
		assert_real(x2, m);
		{float *tmp = x1; x1 = x2; x2 = tmp;} // swap x1 and x2
		box1 = box2;
		eval_dse(data+i*n, x2, &box2, n, h_boxes,
				hg_config.dict_opts.dse.box_min, hg_config.dict_opts.dse.box_max,
				hg_config.dict_opts.dse.order, work0, work1, work2);
//		printf("point %d goes from box %d to %d\n", i, box1, box2);
		for (int j=0; j<m; j++) {
			for (int k=0; k<m; k++) {
				G[box1*m*m + m*j + k] += x1[k]*x1[j];
				int blk = h_blk_idx[box1*bc + box2];
				assert(blk >= 0);
				A[blk*m*m + m*j + k] += x2[k]*x1[j];
			}
		}
//		cblas_sger(CblasColMajor, m, m, 1,
//				x1, 1, x1, 1, G + box1*m*m, m);
//		cblas_sger(CblasColMajor, m, m, 1,
//				x2, 1, x1, 1, A + h_blk_idx[box1*bc+box2]*m*m, m);
	}

	free(x1);
	free(x2);
}

#ifdef GPU
// TODO: this is currently broken and hangs (may just take forever)
// GPU VERSION 0 of formga_dse (simple one update per thread, unstrided atomic adds)
// can't malloc, so needs m x launch_size dimensional work arrays X1 and X2
// and n x launch_size dimensional work arrays w0, w1, w2
// pass n, m, bc, and N as hg_config.n, hg_config.m, hg_box_count, and hg_config.N
// pass an on-GPU version of h_blk_idx, h_boxes, hg_config.dict_opts.dse.box_{min,max} in c_ prefixed args
// adds results to G and A
__global__ void k0_formga_dse(const float *data, float *G, float *A,
		float *X1, float *X2, int n, int m, int bc, int N, float *w0, float *w1, float *w2,
		int *c_blk_idx, struct box *c_boxes, float *c_box_min, float *c_box_max, int order)
{
	int idx = threadIdx.x + blockIdx.x*blockDim.x;

	float *x1 = X1 + m*idx;
	float *x2 = X2 + m*idx;
	int box1, box2; // box x1 and x2 are in

	if (idx < N-1) {
		eval_dse(data+idx*n, x1, &box1, n, c_boxes, c_box_min, c_box_max,
				order, w0+idx*n, w1+idx*n, w2+idx*n);
		eval_dse(data+(idx+1)*n, x2, &box2, n, c_boxes, c_box_min, c_box_max,
				order, w0+idx*n, w1+idx*n, w2+idx*n);
		for (int j=0; j<m; j++) {
			for (int k=0; k<m; k++) {
				atomicAdd(G + box1*m*m + m*j + k,
						x1[k]*x1[j]);
				int blk = c_blk_idx[box1*bc + box2];
				atomicAdd(A + blk*m*m + m*j + k,
						x2[k]*x1[j]);
			}
		}
	}
}
#endif

// HOST VERSION
// calculate RRQR (AP=QR) by Householder with column pivoting
// overwrites A with QR, writes P to vector of integers
// upper triangular bit of A is R, lower triangular is
// Householder reflector vectors with leading component in Q1
// A is m x n in column major order with column stride lda floats
// P and Q1 are length n, stride 1
// give up to avoid catastrophic cancellation if you see a column
// whose norm^2 is less than tol * the max column norm^2 ever
void rrqr(float *A, int m, int n, int lda, int *P, float *Q1, float tol)
{
//	printf("Taking RRQR\n");
	for (int i=0; i<n; i++) {
		P[i] = i;
	}
	float maxevercolnorm2 = 0;
	for (int i=0; i<n-1; i++) { // triangularise columns
//	{ int i=0;
//		printf("i=%d\n", i);
		// pivot
		float maxcolnorm2 = 0; // maximum squared column norm
		float colendnorm2; // column norm excluding ith entry
		int maxcol = -1; // column with maximum norm
		for (int j=i; j<n; j++) { // check column norms
			float *col = A + lda*j;
			assert_real(col, m);
//			printf("col[0]=%f\n", col[0]);
			float colnorm2 = 0;
			float mycolendnorm2 = 0;
			for (int k=i; k<m; k++) {
				colnorm2 += col[k]*col[k];
				if (k > i) {
					mycolendnorm2 += col[k]*col[k];
				}
			}
			if (colnorm2 > maxcolnorm2) {
//				printf("Column %d swapped to since %f > %f\n", j, colnorm2, maxcolnorm2);
				maxcolnorm2 = colnorm2;
				maxcol = j;
				colendnorm2 = mycolendnorm2;
			}
		}
		if (maxcolnorm2 < tol*maxevercolnorm2 || maxcol < 0) {
//			printf("Exact (%g<%g) rank is only %d\n", maxcolnorm2, tol*maxevercolnorm2, i+1);
			for (int j=i; j<n; j++) {
				Q1[j] = 0;
				for (int k=i;k<n; k++) {
					A[lda*j+k] = 0;
				}
			}
			return;
		} else if (maxcolnorm2 > maxevercolnorm2) {
			maxevercolnorm2 = maxcolnorm2;
		}
		int itmp = P[i];
		P[i] = P[maxcol];
		P[maxcol] = itmp;
		for (int j=0; j<m; j++) { // swap columns
//			printf("Pivoting A[%d,%d] to [%d,%d]\n", j, i, j, maxcol);
//			printf("Pivoting column %d to %d\n", i, maxcol);
			float ftmp = A[lda*i + j];
			A[lda*i + j] = A[lda*maxcol + j];
			A[lda*maxcol + j] = ftmp;
		}
		// calculate HH reflector
		float colnorm = sqrtf(maxcolnorm2);
		const float alpha = copysignf(colnorm, A[lda*i+i]);
		Q1[i] = A[lda*i+i] + alpha; // after this, Q1 and lower of A hold v
		assert(colendnorm2 >= 0);
		assert(Q1[i]*Q1[i] >= 0);
		float normv = sqrtf(colendnorm2 + Q1[i]*Q1[i]);
		assert(normv == normv);
		if (normv < tol) {
//			printf("Exact (%g<%g) normv rank is only %d\n", normv, tol, i+1);
			for (int j=i; j<n; j++) {
				Q1[j] = 0;
				for (int k=i;k<n; k++) {
					A[lda*j+k] = 0;
				}
			}
			return;
		}
//		printf("normv = %f\n", normv);
		Q1[i] = Q1[i] / normv;
		for (int j=i+1; j<m; j++) {
//			printf("A[lda*%d + %d] = %f / %f\n", i, j, A[lda*i + j], normv);
			A[lda*i + j] = A[lda*i + j] / normv;
		}
		assert_real(A+lda*i + i+1, m-i-1);
		// apply HH reflector
		A[lda*i + i] = -alpha;
		for (int j=i+1; j<n; j++) { // column
			float utaj = Q1[i]*A[lda*j + i];
			for (int k=i+1; k<m; k++) {
				utaj += A[lda*i + k] * A[lda*j + k];
			}
//			printf("utaj %d = %f\n", j, utaj);
			A[lda*j + i] = A[lda*j + i] - 2*Q1[i]*utaj;
			for (int k=i+1; k<m; k++) {
				A[lda*j + k] -= 2*utaj * A[lda*i + k];
			}
		}
	}
	for (int j=0; j<n; j++) {
		if (j <n-1) {
			assert(P[j] >= 0);
		}
		for (int i=0; i<m; i++) {
			if (A[j*lda+i] != A[j*lda+i]) {
				printf("i=%d j=%d A[j*lda+i]=%f\n", i, j, A[j*lda+i]);
			}
			assert(A[j*lda+i] == A[j*lda+i]);
		}
	}
}

// DEVICE VERSION 0 of bulk rrqr (bonehead one matrix per thread)
// process N A matrices in As into RRQR form with pivots in Ps and first entries of u in Q1s
__global__ void k0_rrqr(int N, float *As, int m, int n, int lda, int *Ps, float *Q1s, float tol)
{
	int idx = threadIdx.x + blockIdx.x*blockDim.x;
	if (idx >= N) return;
	float *A = As + idx*lda*n;
	int *P = Ps + idx*n;
	float *Q1 = Q1s + idx*n;

	for (int i=0; i<n; i++) {
		P[i] = i;
	}
	float maxevercolnorm2 = 0;
	for (int i=0; i<n-1; i++) { // triangularise columns
		// pivot
		float maxcolnorm2 = 0; // maximum squared column norm
		float colendnorm2; // column norm excluding ith entry
		int maxcol = -1; // column with maximum norm
		for (int j=i; j<n; j++) { // check column norms
			float *col = A + lda*j;
			float colnorm2 = 0;
			float mycolendnorm2 = 0;
			for (int k=i; k<m; k++) {
				colnorm2 += col[k]*col[k];
				if (k > i) {
					mycolendnorm2 += col[k]*col[k];
				}
			}
			if (colnorm2 > maxcolnorm2) {
				maxcolnorm2 = colnorm2;
				maxcol = j;
				colendnorm2 = mycolendnorm2;
			}
		}
		if (maxcolnorm2 < tol*maxevercolnorm2 || maxcol < 0) {
			for (int j=i; j<n; j++) {
				Q1[j] = 0;
				for (int k=i;k<n; k++) {
					A[lda*j+k] = 0;
				}
			}
			return;
		} else if (maxcolnorm2 > maxevercolnorm2) {
			maxevercolnorm2 = maxcolnorm2;
		}
		int itmp = P[i];
		P[i] = P[maxcol];
		P[maxcol] = itmp;
		for (int j=0; j<m; j++) { // swap columns
			float ftmp = A[lda*i + j];
			A[lda*i + j] = A[lda*maxcol + j];
			A[lda*maxcol + j] = ftmp;
		}
		// calculate HH reflector
		float colnorm = sqrtf(maxcolnorm2);
		const float alpha = copysignf(colnorm, A[lda*i+i]);
		Q1[i] = A[lda*i+i] + alpha; // after this, Q1 and lower of A hold v
		float normv = sqrtf(colendnorm2 + Q1[i]*Q1[i]);
		if (normv < tol) {
			for (int j=i; j<n; j++) {
				Q1[j] = 0;
				for (int k=i;k<n; k++) {
					A[lda*j+k] = 0;
				}
			}
			return;
		}
		Q1[i] = Q1[i] / normv;
		for (int j=i+1; j<m; j++) {
			A[lda*i + j] = A[lda*i + j] / normv;
		}
		// apply HH reflector
		A[lda*i + i] = -alpha;
		for (int j=i+1; j<n; j++) { // column
			float utaj = Q1[i]*A[lda*j + i];
			for (int k=i+1; k<m; k++) {
				utaj += A[lda*i + k] * A[lda*j + k];
			}
			A[lda*j + i] = A[lda*j + i] - 2*Q1[i]*utaj;
			for (int k=i+1; k<m; k++) {
				A[lda*j + k] -= 2*utaj * A[lda*i + k];
			}
		}
	}
}

// maximum matrix size k1_rrqr can handle
#define MAX_RRQR_m 6

// KERNEL VERSION 1 (thread handles row, group handles problem)
// assumes m == n == lda
// requires 2*m words (2 set is floats, 1 set is ints) of shared memory
__global__ void __launch_bounds__(MAX_RRQR_m) k1_rrqr(int N, float *c_As, int m, int *c_Ps, float *c_Q1s)
{
	extern __shared__ float shmem[];
	__shared__ float s_alpha, s_beta;
	float *c_A = c_As + blockIdx.x*m*m;
	int *c_P = c_Ps + blockIdx.x*m;
	float *c_Q1 = c_Q1s + blockIdx.x*m;
	int row = threadIdx.x;

	float *s_norms = shmem; // used in finding pivot
	float *s_utajs = shmem; // used in applying Householder
	int *s_P = (int*) shmem + m;
	float a[MAX_RRQR_m];

	for (int i=0; i<m; i++) {
		a[i] = c_A[i*m + row];
	}
	s_P[row] = row; // not used for a while, uses __syncthreads in column norm reduction

	for (int i=0; i<m-1; i++) {
		// find pivot
		s_norms[row] = 0;
		__syncthreads();
		for (int j=i; j<m; j++) { // striped similtaneous reduction of colunm norms
			int mycol = i + (row+j)%(m-i);
			if (row >= i) {
				s_norms[mycol] += a[mycol]*a[mycol];
			}
			__syncthreads(); // here to sync for finding max norm column
		}
		float maxnorm = 0; // independently found by all threads
		int pivcol = 0;
		for (int j=i; j<m; j++) {
			if (s_norms[j] > maxnorm*1.1) { // buff keeping the same column to prefer low orders
				if (row == i) {
//					printf("Column %d has norm^2 %f > %f\n", j, s_norms[j], maxnorm);
				}
				maxnorm = s_norms[j];
				pivcol = j;
			}
		}
		if (maxnorm < 1e-30) {
//			if (row == i) printf("Max %dth column norm is %f, truncating\n", i, maxnorm);
			pivcol = i;
			if (row >= i) {
				for (int j=i; j<m; j++) {
					a[j] = 0;
				}
			}
			__syncthreads();
			break;
		}
		// apply pivot
		if (row == i) {
//			printf("Pivoting %d to %d\n", i, pivcol);
			int tmp = s_P[i];
			s_P[i] = s_P[pivcol];
			s_P[pivcol] = tmp;
		}
		{
//			printf("Pivoting a[%d,%d] to [%d,%d]\n", row, i, row, pivcol);
			float tmp = a[i];
			a[i] = a[pivcol];
			a[pivcol] = tmp;
		}
		// find Householder
		float gamma; // first entry of reflector, used later as row i in applying
		if (row == i) {
			s_alpha = copysignf(sqrtf(maxnorm), a[i]);
			gamma = a[i] + s_alpha;
			s_beta = sqrtf(maxnorm + s_alpha*(2*a[i] + s_alpha));
//			printf("beta = %f\n", s_beta);
			gamma = c_Q1[i] = gamma / s_beta;
		}
		__syncthreads();
		if (row > i) {
			a[i] = a[i] / s_beta;
		}
		// apply Householder
		if (row == i) {
			a[i] = -s_alpha;
		}
		float u = (row == i) ? gamma : (row > i) ? a[i] : 0;
		s_utajs[row] = 0;
		for (int j=0; j<m; j++) {
			__syncthreads();
			int mycol = (row+j) % m;
//			printf("s_utajs[%d] (%f) += %f*%f (row %d)\n", mycol, s_utajs[mycol], a[mycol], u, row);
			s_utajs[mycol] += a[mycol]*u;
		}
		__syncthreads();
		for (int j=i+1; j<m; j++) {
//			if (row == i) printf("s_utajs[%d] = %f\n", j, s_utajs[j]);
			float u = (row == i) ? gamma : (row > i) ? a[i] : 0;
			a[j] -= 2*u*s_utajs[j];
		}
	}

	for (int i=0; i<m; i++) {
		if (row == 0) {
			c_P[i] = s_P[i];
		}
		c_A[i*m + row] = a[i];
	}
}

// HOST VERSION
// use RRQR truncated to approximate 2-condition number tol
// to solve block linear system QRP'X = A where everything is m x m
// puts solution X into top of A, permuted by Xr
// note that Xr depends only on QR, P, and tol and may therefore be reused
// uses a m dimensional work vector, which cannot contain nan or infinity
void rrqr_ls(int m, const float *QR, const float *Q1, const int *P,
		float *A, float tol, int *rank, float *work)
{
	// find truncation amount
	*rank = m;
	float r11 = fabs(QR[0]);
	for (int i=1; i<m-1; i++) {
		if (fabs(QR[i*m + i]) < tol*r11) {
			*rank = i;
			break;
		}
	}
	// apply HH to A
	// the operation is (I-2uu')A = A - 2uu'A
	for (int i=0; i<*rank; i++) {
		if (i == m-1) break;
		// dot product columns with u to calculate work = u'A = (A'u)'
		// only past i, because the previous stuff is zero in u
		for (int j=0; j<m; j++) {
			work[j] = Q1[i] * A[j*m + i];
			for (int k=i+1; k<m; k++) { // for later rows
				work[j] += QR[i*m + k] * A[j*m + k];
			}
//			printf("work[%d] = %f\n", j, work[j]);
		}
		// make rank-1 update A = A - 2uu'A = A - 2u work'
		for (int j=0; j<m; j++) {
			A[j*m + i] -= 2*Q1[i] * work[j];
			for (int k=i+1; k<m; k++) {
				A[j*m + k] -= 2 * QR[i*m + k] * work[j];
			}
		}
	}
//	print_matrix(A, m, m);
	// solve triangular system RX = A
	for (int i=0; i<m; i++) { // columns of A
		for (int j=*rank-1; j>=0; j--) { // columns of R
			A[i*m + j] = A[i*m + j] / QR[j*m + j];
			for (int k=0; k<j; k++) { // rows of R
				A[i*m + k] -= QR[j*m + k] * A[i*m + j];
			}
		}
	}
	for (int j=0; j<m; j++) {
		for (int i=*rank; i<m; i++) {
			A[j*m + i] = 0;
		}
	}
	// permute rows, column by column
	for (int i=0; i<m; i++) {
		memcpy(work, A+i*m, m*sizeof(float));
		for (int j=0; j<m; j++) {
			A[i*m + P[j]] = work[j];
		}
	}
}

// DEVICE VERSION 0 of bulk rrqr_ls (one system per thread)
// there are N systems, each with a unique A in As
// but only M matrices to pseudoinvert, in QRs, Q1s, and Ps
// this is indirected through QRidx
// ranks is only size M, written atomically
// works is a m*N work vector
__global__ void k0_rrqr_ls(int N, int M, int *QRidx,
		int m, const float *QRs, const float *Q1s, const int *Ps,
		float *As, float tol, int *ranks, float *works)
{
	int idx = threadIdx.x + blockIdx.x*blockDim.x;
	if (idx >= N) return;
	const float *QR = QRs + QRidx[idx]*m*m;
	const float *Q1 = Q1s + QRidx[idx]*m;
	int *c_rank = ranks + QRidx[idx];
	const int *P = Ps + QRidx[idx]*m;
	float *A = As + idx*m*m;
	float *work = works + idx*m;

	// find truncation amount
	int rank = m;
	float r11 = fabs(QR[0]);
	for (int i=1; i<m; i++) {
		if (fabs(QR[i*m + i]) < tol*r11) {
			rank = i;
			break;
		}
	}
	// apply HH to A
	// the operation is (I-2uu')A = A - 2uu'A
	for (int i=0; i<rank; i++) {
		if (i == m-1) break;
		// dot product columns with u to calculate work = u'A = (A'u)'
		// only past i, because the previous stuff is zero in u
		for (int j=0; j<m; j++) {
			work[j] = Q1[i] * A[j*m + i];
			for (int k=i+1; k<m; k++) { // for later rows
				work[j] += QR[i*m + k] * A[j*m + k];
			}
		}
		// make rank-1 update A = A - 2uu'A = A - 2u work'
		for (int j=0; j<m; j++) {
			A[j*m + i] -= 2*Q1[i] * work[j];
			for (int k=i+1; k<m; k++) {
				A[j*m + k] -= 2 * QR[i*m + k] * work[j];
			}
		}
	}
	// solve triangular system RX = A
	for (int i=0; i<m; i++) { // columns of A
		for (int j=rank-1; j>=0; j--) { // columns of R
			A[i*m + j] = A[i*m + j] / QR[j*m + j];
			for (int k=0; k<j; k++) { // rows of R
				A[i*m + k] -= QR[j*m + k] * A[i*m + j];
			}
		}
	}
	for (int j=0; j<m; j++) {
		for (int i=rank; i<m; i++) {
			A[j*m + i] = 0;
		}
	}
	// permute rows, column by column
	for (int i=0; i<m; i++) {
		for (int j=i; j<m; j++) {
			work[j] = A[i*m + j];
		}
		for (int j=0; j<m; j++) {
			A[i*m + P[j]] = work[j];
		}
	}
	// save rank
	atomicMin(c_rank, rank);
}

// DEVICE VERSION 1 of rrqr_ls (complete rewrite to be smart about GPU)
// the launch parameters are in 3D
// the x dimension selects the problem
// the y dimension selects a column of A
// the z dimension selects a row (of R, u, and A)
// there should be only one thread group in the y and z dimension
// but any number of thread groups and thread dimension can be in the x dimension
// needs dynamic shared memory with the blockDim.x * m floats
// this is used for what was the work vector
// must run with at least one problem to solve
__global__ void k1_rrqr_ls(int N, int M, int *c_QRidx,
		int m, const float *c_QRs, const float *c_Q1s, const int *c_Ps,
		float *c_As, float tol)
{
	extern __shared__ float shmem[];

	// figure out the problem
	int idx = threadIdx.x + blockIdx.x*blockDim.x;
	int QRidx = (idx < N) ? c_QRidx[idx] : 0;
	const float *c_QR = c_QRs + QRidx*m*m;
	const float *c_Q1 = c_Q1s + QRidx*m;
	const int *c_P = c_Ps + QRidx*m;
	float *c_A = c_As + ((idx < N) ? idx*m*m : 0);
	float *s_work = shmem + threadIdx.x*m;

	// figure out where we fit in the problem
	int col = threadIdx.y;
	int row = threadIdx.z;
	float a = c_A[col*m + row];

//	printf("QR[%d,%d] = %f\n", row, col, c_QR[col*m + row]);

	// figure out the condition of the problem (and truncate)
	__shared__ int s_rank;
	if (col == 0 && row == 0) s_rank = m+1;
	__shared__ float s_ansf;
	if (col == 0 && row == 0) s_ansf = fabsf(c_QR[0]);
	__syncthreads();
	if (col == row && fabsf(c_QR[col*m+row]) < tol*s_ansf) atomicMin(&s_rank, col);
	__syncthreads();
	int rank = s_rank-1;
	if (row == rank-1 && col == rank-1) {
//		printf("Rank %d, conditioning %f after truncation.\n", rank, s_ansf / fabsf(c_QR[col*m+row]));
	}

	// reflect on Householders
	for (int i=0; i<rank; i++) {
		// TODO: horrible serial reduction for matrix multiply
		if (row == i) {
			s_work[col] = a * c_Q1[i];
		}
		for (int j=i+1; j<m; j++) {
			__syncthreads();
			if (row == j) {
				s_work[col] += a * c_QR[i*m + j];
			}
		}
		__syncthreads();
//		printf("s_work[%d] = %f\n", col, s_work[col]);
		// rank-1 update
		float u = (row == i) ? c_Q1[i] : (row > i) ? c_QR[i*m+row] : 0;
		a = a - 2 * u * s_work[col];
	}

//	printf("Pre-backsub a[%d,%d] = %f\n", row, col, a);

	// perform backward substitution
	for (int i=rank-1; i>=0; i--) {
//		if (row == 0 && col == 0) printf("a[0,0] = %f\n", a);
//		if (row == 1 && col == 0) printf("a[1,0] = %f\n", a);
		if (row == i) {
			a = a / c_QR[i*m+i];
			s_work[col] = a;
		}
//		if (row == 1 && col == 0) printf("a[1,0] = %f\n", a);
		__syncthreads();
//		if (row == 0 && col == 0) printf("s_work[0] = %f\n", s_work[0]);
		if (row < i) {
			a = a - c_QR[i*m+row] * s_work[col];
//			if (col == 0) printf("c_QR[%d*m+%d] = %f\n", i, row, c_QR[i*m+row]);
		}
	}
	if (row >= rank) a = 0;


	// write out results
	if (idx < N) c_A[col*m + c_P[row]] = a;
}

// HOST VERSION
// form K under dse
// returns K under blocks as in Kblk, but only using rows as in Kr (per block row, local offset)
// the rank of each block is in ranks
void formk_dse(const float *data, float *K, int *ranks)
{
	int m = hg_config.m;
	int bc = hg_box_count;

	clock_t t0 = clock();
	float *G = (float*) malloc(sizeof(float) * m*m*bc);
	formga_dse(data, G, K);
	clock_t t1 = clock();
	printf("% 10.6gs to form G and A from data\n", (t1-t0) / (float) CLOCKS_PER_SEC);

	float *Q1s = (float*) malloc(sizeof(float) * m*bc);
	int *Ps = (int*) malloc(sizeof(int) * m*bc);
	for (int i=0; i<bc; i++) {
		rrqr(G+i*m*m, m, m, m, Ps+i*m, Q1s+i*m, 1e-7);
//		puts("G^{-1}:");
//		print_matrix(G+i*m*m, m, m);
	}
	clock_t t2 = clock();
	printf("% 10.6gs to take QR decomposition of G\n", (t2-t1) / (float) CLOCKS_PER_SEC);

	float *work = (float*) calloc(sizeof(float), m);
	for (int i=0; i<hg_box_adjs; i++) {
		int br = h_idx_blk[i*2];
		assert(br >= 0);
		rrqr_ls(m, G + br*m*m, Q1s + br*m, Ps + br*m,
			K + i*m*m, 1e-5, ranks + br, work);
//		puts("K:");
//		print_matrix(K + i*m*m, m, m);
	}
	clock_t t3 = clock();
	printf("% 10.6gs to GK=A least squares\n", (t3-t2) / (float) CLOCKS_PER_SEC);

	free(G);
	free(Q1s);
	free(Ps);
	free(work);
}

// partial driver for device verison
// TODO: c_ranks is never used; I need to remove it
void gpu_formk_dse(const float *h_data, float *c_K, int *c_ranks,
		int *c_blk_idx, struct box *c_boxes, float *c_box_min, float *c_box_max)
{
	int N = hg_config.N;
	int m = hg_config.m;
	int bc = hg_box_count;
	const int batch_size = 1000000;
	int bs = min(batch_size, N);
	cudaEvent_t tic, trrqr, toc;
	cudaEventCreate(&tic);
	cudaEventCreate(&trrqr);
	cudaEventCreate(&toc);

	/* code to run buggy k0_forgma_dse
	float *c_G, *c_work, *c_data;
	cudaMalloc(&c_data, sizeof(float) * n*N);
	cudaMemcpy(c_data, h_data, sizeof(float) * n*N, cudaMemcpyHostToDevice);
	cudaMalloc(&c_G, sizeof(float) * m*m*bc);
	cudaMalloc(&c_work, sizeof(float) * max((m+m+n+n+n)*bs, m*bc+m*hg_box_adjs));
	for (int i=0; i<N; i+=bs) {
		k0_formga_dse <<< (bs+256-1)/256 , 256 >>>
				(c_data+i*n, c_G, c_K, c_work+0*m*bs+0*n*bs, c_work+1*m*bs+0*n*bs,
				n, m, bc, N, c_work+2*m*bs+0*n*bs, c_work+2*m*bs+1*n*bs, c_work+2*m*bs+2*n*bs,
				c_blk_idx, c_boxes, c_box_min, c_box_max, hg_config.dict_opts.dse.order);
	}
	*/

	clock_t t0 = clock();
	float *h_A = (float*) malloc(sizeof(float) * hg_box_adjs*m*m);
	float *h_G = (float*) malloc(sizeof(float) * bc*m*m);
	formga_dse(h_data, h_G, h_A);
	float *c_G, *c_work;
	cudaMalloc(&c_G, sizeof(float) * m*m*bc);
	cudaMalloc(&c_work, sizeof(float) * (m*bc+m*hg_box_adjs)); // TODO: shrink this
	cudaMemcpy(c_G, h_G, sizeof(float) * bc*m*m, cudaMemcpyHostToDevice);
	cudaMemcpy(c_K, h_A, sizeof(float) * hg_box_adjs*m*m, cudaMemcpyHostToDevice);
	clock_t t1 = clock();
	printf("% 10.6gs to form and upload G and A to GPU\n", (t1-t0) / (float) CLOCKS_PER_SEC);

	cudaMemcpy(h_G, c_G, sizeof(float) * m*m*bc, cudaMemcpyDeviceToHost);
	assert_real(h_G, m*m*bc);
//	print_matrix(h_G, m, m);

	cudaEventRecord(tic);
	float *c_Q1s = c_work;
	int *c_Ps, *c_QRidx;
	cudaMalloc(&c_Ps, sizeof(int) * m*bc);
	cudaMalloc(&c_QRidx, sizeof(int) * hg_box_adjs);
//	k0_rrqr <<< (bc+256-1)/256 , 256 >>> (bc, c_G, m, m, m, c_Ps, c_Q1s, 1e-7);
	k1_rrqr <<< bc , m , 2*m*sizeof(float) >>> (bc, c_G, m, c_Ps, c_Q1s);
	cudaEventRecord(trrqr);

	puts("G^{-1}:");
	cudaMemcpy(h_G, c_G, sizeof(float) * m*m*bc, cudaMemcpyDeviceToHost);
	assert_real(h_G, m*m*bc);
	print_matrix(h_G, m, m);
//	cudaMemcpy(h_G, c_Q1s, sizeof(float) * m, cudaMemcpyDeviceToHost);
//	print_matrix(h_G, 1, m);

	int *h_QRidx = (int*) malloc(sizeof(int) * hg_box_adjs);
	for (int i=0; i<hg_box_adjs; i++) {
		int br = h_idx_blk[i*2];
		assert(br >= 0);
		h_QRidx[i] = br;
	}
	cudaMemcpy(c_QRidx, h_QRidx, sizeof(int) * hg_box_adjs, cudaMemcpyHostToDevice);
//	k0_rrqr_ls <<< (hg_box_adjs+256-1)/256 , 256 >>> (hg_box_adjs, bc, c_QRidx, m,
//			c_G, c_Q1s, c_Ps, c_K, 1e-5, c_ranks, c_work+m*bc);
	dim3 qrls_G(hg_box_adjs);
	dim3 qrls_B(1, m, m);
	k1_rrqr_ls <<< qrls_G, qrls_B, sizeof(float)*m >>> (hg_box_adjs, bc, c_QRidx, m,
			c_G, c_Q1s, c_Ps, c_K, 1e-5);
	cudaEventRecord(toc);

	puts("K:");
	cudaMemcpy(h_A, c_K, sizeof(float) * m*m*hg_box_adjs, cudaMemcpyDeviceToHost);
	print_matrix(h_A, m, m);
	assert_real(h_A, m*m*hg_box_adjs);

	cudaDeviceSynchronize();
	float elapsed;
	cudaEventElapsedTime(&elapsed, tic, trrqr);
	printf("% 10.6gs to find RRQR on GPU\n", elapsed/1000);
	cudaEventElapsedTime(&elapsed, trrqr, toc);
	printf("% 10.6gs to solve least squares with RRQR on GPU\n", elapsed/1000);

	cudaFree(c_G);
	cudaFree(c_work);
	cudaFree(c_Ps);
	cudaFree(c_QRidx);
	free(h_QRidx);
	free(h_G);
	free(h_A);
}

// HOST VERSION
// apply block sparse A to a vector x, overwriting it
void bspmvm(const float *A, const int *ranks, const float *x, float *out)
{
	int m = hg_config.m;
	memset(out, 0, sizeof(float)*m*hg_box_count);

	// do the multiplications
	for (int i=0; i<hg_box_adjs; i++) {
		assert(ranks[h_idx_blk[2*i]] > 0);
		int b0 = h_idx_blk[2*i]; // from box
		int b1 = h_idx_blk[2*i+1]; // to box
		for (int j=0; j<m; j++) { // entries of input
			for (int k=0; k<m; k++) { // entries of output
				out[b0*m + k] += A[i*m*m + j*m + k] * x[b1*m + j];
			}
		}
	}
}

// DEVICE VERSION 0 (group handles box adjacency, non-full usage)
// currently uses atomics to make actual update
// probably a better box-to-index datastructure is needed
__global__ void k0_bspmvm(const float *A, const float *x, float *out,
		int N, int m, int adjs, int *c_idx_blk)
{
	int t = threadIdx.x;
	int b = blockIdx.x;
	if (t < m && b < adjs) {
		int b0 = c_idx_blk[2*b];
		int b1 = c_idx_blk[2*b+1];
		float my_contrib = 0;
		for (int j=0; j<m; j++) {
			my_contrib += A[b*m*m + j*m + t] * x[b1*m + j];
//			atomicAdd(out+b0*m+t, A[b*m*m + j*m + t] * x[b1*m + j]);
		}
		atomicAdd(out + b0*m + t, my_contrib);
	}
}

// DEVICE VERSION 1 (multiple adjacencies per group)
// then atomics to make actual update
// still should probably use better box-to-index
__global__ void k1_bspmvm(const float *A, const float *x, float *out,
		int N, int m, int adjs, int *c_idx_blk)
{
	int t = threadIdx.x % m;
	int b = blockIdx.x*(blockDim.x/m) + threadIdx.x/m;
	if (t < m && b < adjs) {
		int b0 = c_idx_blk[2*b];
		int b1 = c_idx_blk[2*b+1];
		float my_contrib = 0;
		for (int j=0; j<m; j++) {
			my_contrib += A[b*m*m + j*m + t] * x[b1*m + j];
		}
		atomicAdd(out + b0*m + t, my_contrib);
	}
}

// HOST VERISON
// run power method on block sparse to find dominant eigenvalue
// tol uses infinity norm residual
// x is unit wrt the 2-norm
void eigpower_bsp(const float *A, const int *ranks,
		float *x, float *lambda, float tol, int maxsteps)
{
	int m = hg_config.m;
	int M = m*hg_box_count;
	float *xold = (float*) malloc(sizeof(float) * M);
	for (int i=0; i<M; i++) {
		x[i] = drand48();
	}
	float resid;
	int step = 0;
	do {
		float x2 = 0; // will do dot product, probably make a kernel
		for (int i=0; i<M; i++) {
			x2 += x[i] * x[i];
		}
		assert(x2==x2);
		assert(x2+1e38!=x2);
		memcpy(xold, x, sizeof(float) * M);
		bspmvm(A, ranks, xold, x);
		float xAx = 0; // another dot product
		for (int i=0; i<M; i++) {
			xAx += xold[i] * x[i];
		}
		*lambda = xAx/x2;
		resid = 0; // distance to eigenvalue, infty-norm
		for (int i=0; i<M; i++) { // probably make kernel
			float r = fabs(*lambda*xold[i] - x[i]);
//			if (r > resid) resid = r; // infty-norm, harder reduction than 1-norm
			resid += r; // 1-norm, can use atomics
		}
		for (int i=0; i<M; i++) { // scalar multiply, probably make kernel
			x[i] = x[i] / sqrt(x2);
		}
		step++;
	} while (resid > tol && step < maxsteps);
	printf("Power method got %f at residual %f in %d steps\n", *lambda, resid, step);
	free(xold);
}

#define RED_B 1024

// n dimensional dot product
// based on reductionKernelV5 from the notes
// TODO: see if can rework to be FMAdd based (in serial it is)
__global__ void k0_dot(int N, const float *x, const float *y, float *r)
{
	__shared__ float s_xy[RED_B];
	int t = threadIdx.x;
	int n = threadIdx.x + blockIdx.x*blockDim.x;
	s_xy[t] = (n < N) ? x[n]*y[n] : 0;
	int alive = RED_B/2;
	while (alive > 0) {
		__syncthreads();
		if (t < alive) {
			s_xy[t] += s_xy[t + alive];
		}
		alive /= 2;
	}
	if (t == 0) {
		atomicAdd(r, s_xy[0]);
	}
}

// n dimensional 1-norm based absolute eigenvalue residual
// basically copy pasted from k0_dot
__global__ void k0_err_1norm(int N, const float *x, const float *Ax, float lambda, float *r)
{
	__shared__ float s_x[RED_B];
	int t = threadIdx.x;
	int n = threadIdx.x + blockIdx.x*blockDim.x;
	s_x[t] = (n < N) ? fabs(Ax[n] - lambda*x[n]) : 0;
	int alive = RED_B/2;
	while (alive > 0) {
		__syncthreads();
		if (t < alive) {
			s_x[t] += s_x[t + alive];
		}
		alive /= 2;
	}
	if (t == 0) {
		atomicAdd(r, s_x[0]);
	}
}

// scale an n dimensional vector
__global__ void k0_scale(int N, float *x, float alpha)
{
	int n = threadIdx.x + blockIdx.x*blockDim.x;
	if (n < N) x[n] = x[n] * alpha;
}

// GPU PARTIAL DRIVER
void gpu_eigpower_bsp(const float *c_A,
		float *c_x, float *lambda, float tol, int maxsteps,
		int *c_idx_blk)
{
	int m = hg_config.m;
	int M = m*hg_box_count;
	float *c_xold;
	cudaMalloc(&c_xold, sizeof(float) * M);
	float *h_x = (float*) malloc(sizeof(float) * M);
	for (int i=0; i<M; i++) {
		h_x[i] = drand48();
	}
	cudaMemcpy(c_x, h_x, sizeof(float) * M, cudaMemcpyHostToDevice);
	float resid;
	int step = 0;
	float *c_dot;
	cudaMalloc(&c_dot, sizeof(float));
	do {
		float x2;
		cudaMemset(c_dot, 0, sizeof(float));
		k0_dot <<< (M+256-1)/256 , 256 >>> (M, c_x, c_x, c_dot);
		cudaMemcpy(&x2, c_dot, sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(c_xold, c_x, sizeof(float) * M, cudaMemcpyDeviceToDevice);
		cudaMemset(c_x, 0, sizeof(float) * M);
		k1_bspmvm <<< (hg_box_adjs+16-1)/16 , m*16 >>> (c_A, c_xold, c_x, hg_config.N, m, hg_box_adjs, c_idx_blk);

//		puts("x:");
//		cudaMemcpy(h_x, c_xold, sizeof(float)*M, cudaMemcpyDeviceToHost);
//		print_matrix(h_x, 1, M);
//		puts("Ax:");
//		cudaMemcpy(h_x, c_x, sizeof(float)*M, cudaMemcpyDeviceToHost);
//		print_matrix(h_x, 1, M);
//		printf("\\|x\\|_2^2 = %f\n", x2);

		float xAx;
		cudaMemset(c_dot, 0, sizeof(float));
		k0_dot <<< (M+256-1)/256 , 256 >>> (M, c_xold, c_x, c_dot);
		cudaMemcpy(&xAx, c_dot, sizeof(float), cudaMemcpyDeviceToHost);
		*lambda = xAx/x2;

//		printf("x^TAx = %f\n", xAx);

		resid = 0; // distance to eigenvalue, infty-norm
		cudaMemset(c_dot, 0, sizeof(float));
		k0_err_1norm <<< (M+256-1)/256 , 256 >>> (M, c_x, c_xold, *lambda, c_dot);
		cudaMemcpy(&resid, c_dot, sizeof(float), cudaMemcpyDeviceToHost);

		k0_scale <<< (M+256-1)/256 , 256 >>> (M, c_x, 1/sqrt(x2));

		step++;
	} while (resid > tol && step < maxsteps);

	printf("Power method got %f at residual %f in %d steps\n", *lambda, resid, step);
	cudaFree(c_xold);
	cudaFree(c_dot);
	free(h_x);
}

// unit test of rrqr to solve linear system
void test_rrqr(int verbose)
{
	printf("Starting RRQR Test.\n");
	int m = 10;
	int n = 10;
	float *A = (float*) malloc(m*n*sizeof(float));
	float *A0 = (float*) malloc(m*n*sizeof(float));
	float *B = (float*) malloc(n*m*sizeof(float));
	float *B0 = (float*) malloc(n*m*sizeof(float));
	float *C = (float*) calloc(n*m, sizeof(float));
	int *P = (int*) malloc(n*sizeof(int));
	float *Q1 = (float*) malloc(n*sizeof(int));
	float *work = (float*) malloc(n*sizeof(float));
	int rank;
retry:
	for (int i=0; i<n; i++) {
		for (int j=0; j<m; j++) {
			A[i*m + j] = drand48();
			if (i == j) {
				A[i*m + j] += 1e3;
			}
//			if (i == j) {
//				A[i*m + j] = i+1;
//			} else {
//				A[i*m + j] = 0;
//			}
			A0[i*m + j] = A[i*m + j];
//			printf("%f ", A[i*m + j]);
			B[i*m + j] = drand48();
//			B[i*m + j] = i*m + j;
//			B[i*m + j] = A[i*m + j];
			B0[i*m + j] = B[i*m + j];
		}
//		printf("\n");
	}
	if (verbose) {
		puts("A:");
		print_matrix(A, m, n);
	}
	rrqr(A, m, n, m, P, Q1, 1e-7);
	if (verbose) {
		puts("A:");
		print_matrix(A, m, n);
		puts("Q1:");
		print_matrix(Q1, 1, n);
		puts("P:");
		for (int i=0; i<n; i++) printf("%d ", P[i]);
		printf("\n");
	}
	rrqr_ls(m, A, Q1, P, B, 1e-3, &rank, work);
	if (rank < m) {
		printf("Retrying due to low-rank %d<%d\n", rank, m);
		assert(false);
		goto retry;
	}
	// check
	memset(C, 0, sizeof(float)*m*m);
	for (int i=0; i<m; i++) { // columns of input B (and output C)
		for (int j=0; j<m; j++) { // rows of input A0 (and output C)
			for (int k=0; k<m; k++) { // shared dimension
				C[i*m + j] += A0[k*m + j] * B[i*m + k];
			}
		}
	}
	if (verbose) {
		puts("B:");
		print_matrix(B, n, m);
		puts("B0:");
		print_matrix(B0, m, n);
		puts("C:");
		print_matrix(C, m, n);
	}
	for (int i=0; i<n; i++) {
		for (int j=0; j<m; j++) {
			assert(fabs(C[i*m + j] - B0[i*m + j]) < 0.1);
		}
	}
	printf("RRQR Test Passed\n");
}

// HOST VERSION
// driver code to call functions and handle GPU memory movement
// stuff that can be returned only return if not NULL
// can return Koopman matrix in K_out (with ranks in ranks_out)
// can return principal Koopman eigenfunction weights in ev_out
// and eigenvalue in lambda_out
void edmd_driver(const float *data, float **K_out, int **ranks_out,
		float **ev_out, float *lambda_out)
{
	clock_t t0 = clock();
	setup_dict(data);
	clock_t t1 = clock();
	printf("% 10.6gs to setup dictionary\n", (t1-t0) / (float) CLOCKS_PER_SEC);

	int m = hg_config.m;
	float *K = (float*) malloc(sizeof(float) * m*m*hg_box_adjs);
	int *ranks = (int*) malloc(sizeof(int) * m*hg_box_count);
	formk_dse(data, K, ranks);
	clock_t t2 = clock();
	printf("% 10.6gs total to form Koopman matrix\n", (t2-t1) / (float) CLOCKS_PER_SEC);

	float *ev = (float*) malloc(sizeof(float) * m*hg_box_count);
	float lambda;
	eigpower_bsp(K, ranks, ev, &lambda, 1e-5, 100000);
	clock_t t3 = clock();
	printf("% 10.6gs to form run power method\n", (t3-t2) / (float) CLOCKS_PER_SEC);

	if (K_out) {
		*K_out = K;
	} else {
		free(K);
	}
	if (ranks_out) {
		*ranks_out = ranks;
	} else {
		free(ranks);
	}
	if (ev_out) {
		*ev_out = ev;
	} else {
		free(ev);
	}
	if (lambda_out) {
		*lambda_out = lambda;
	}
}

// GPU VERSION of edmd_driver
// handles all GPU details, drop in replacement
void gpu_edmd_driver(const float *data, float **K_out, int **ranks_out,
		float **ev_out, float *lambda_out)
{
	clock_t ht0 = clock();
	setup_dict(data);
	int m = hg_config.m;
	int *c_blk_idx;
	cudaMalloc(&c_blk_idx, sizeof(int) * hg_box_count*hg_box_count);
	cudaMemcpy(c_blk_idx, h_blk_idx, sizeof(int) * hg_box_count*hg_box_count, cudaMemcpyHostToDevice);
	int *c_idx_blk;
	cudaMalloc(&c_idx_blk, sizeof(int) * hg_box_adjs*2);
	cudaMemcpy(c_idx_blk, h_idx_blk, sizeof(int) * hg_box_adjs*2, cudaMemcpyHostToDevice);
	struct box *c_boxes;
	cudaMalloc(&c_boxes, sizeof(struct box) * hg_box_count);
	cudaMemcpy(c_boxes, h_boxes, sizeof(struct box) * hg_box_count, cudaMemcpyHostToDevice);
	float *c_box_min, *c_box_max;
	cudaMalloc(&c_box_min, sizeof(float) * hg_config.n);
	cudaMalloc(&c_box_max, sizeof(float) * hg_config.n);
	cudaMemcpy(c_box_min, hg_config.dict_opts.dse.box_min, sizeof(float)*hg_config.n, cudaMemcpyHostToDevice);
	cudaMemcpy(c_box_max, hg_config.dict_opts.dse.box_max, sizeof(float)*hg_config.n, cudaMemcpyHostToDevice);
	clock_t ht1 = clock();
	printf("% 10.6gs to create dictionary and send to GPU\n", (ht1-ht0) / (float) CLOCKS_PER_SEC);

	float *c_K;
	cudaMalloc(&c_K, sizeof(float) * m*m*hg_box_adjs);
	int *c_ranks;
	cudaMalloc(&c_ranks, sizeof(int) * m*hg_box_count);
	gpu_formk_dse(data, c_K, c_ranks, c_blk_idx, c_boxes, c_box_min, c_box_max);
	clock_t ht2 = clock();
	printf("% 10.6gs total to form Koopman approximation matrix\n", (ht2-ht1) / (float) CLOCKS_PER_SEC);

	float *c_ev;
	cudaMalloc(&c_ev, sizeof(float) * m*hg_box_count);
	float lambda;
	gpu_eigpower_bsp(c_K, c_ev, &lambda, 1e-5*hg_box_count, 100000, c_idx_blk);
	clock_t ht3 = clock();
	printf("% 10.6gs to run power method\n", (ht3-ht2) / (float) CLOCKS_PER_SEC);

	if (K_out) {
		float *h_K = (float*) malloc(sizeof(float) * m*m*hg_box_adjs);
		cudaMemcpy(h_K, c_K, sizeof(float) * m*m*hg_box_adjs, cudaMemcpyDeviceToHost);
		*K_out = h_K;
	}
	if (ranks_out) {
		int *h_ranks = (int*) malloc(sizeof(int) * m*hg_box_count);
		cudaMemcpy(h_ranks, c_ranks, sizeof(int) * m*hg_box_count, cudaMemcpyDeviceToHost);
		*ranks_out = h_ranks;
	}
	if (ev_out) {
		float *h_ev = (float*) malloc(sizeof(float) * m*hg_box_count);
		cudaMemcpy(h_ev, c_ev, sizeof(float) * m*hg_box_count, cudaMemcpyDeviceToHost);
		*ev_out = h_ev;
	}
	if (lambda_out) {
		*lambda_out = lambda;
	}

	cudaFree(c_ev);
	cudaFree(c_ranks);
	cudaFree(c_K);
	cudaFree(c_box_max);
	cudaFree(c_box_min);
	cudaFree(c_boxes);
	cudaFree(c_idx_blk);
	cudaFree(c_blk_idx);
}

// test recovery of a linear system
void test_linear(void)
{
	int N = 99999;
	printf("Starting Linear Test.\n");
	float *x = (float*) malloc(sizeof(float) * 2*(N+2));
	x[0] = drand48()+1; x[1] = drand48()+0.1;
	for (int i=0; i<N+1; i++) {
		assert_real(x+2*i, 2);
		x[2*i+2] = x[2*i+0] * 0.99;
		x[2*i+3] = x[2*i+1] * 1;
	}
	hg_config.dict = D_DSE;
	hg_config.dict_opts.dse.order = 2;
	hg_config.dict_opts.dse.box_size = 10;
	hg_config.dict_opts.dse.box_min = (float*) calloc(sizeof(float), 2);
	hg_config.dict_opts.dse.box_max = (float*) malloc(sizeof(float)*2);
	hg_config.dict_opts.dse.box_max[0] = x[0];
	hg_config.dict_opts.dse.box_max[1] = x[1];
//	printf("%f ", hg_config.dict_opts.dse.box_max[0]);
//	printf("%f\n", hg_config.dict_opts.dse.box_max[1]);
	hg_config.N = N;
	hg_config.n = 2;
	hg_config.tol = 1e-3;
	hg_config.pinv = PI_RRQR;

	float *K;
	float *ev;
	float lambda;
	gpu_edmd_driver(x, &K, NULL, &ev, &lambda);
//	edmd_driver(x, &K, NULL, &ev, &lambda);

//	puts("K:");
//	print_matrix(K, hg_config.m, hg_config.m);

	printf("Eigenvalue (should be 1.0) estimated as %f\n", lambda);
	assert(lambda >= 0.99 && lambda <= 1.01);

	free(K);
	free(ev);
	printf("Linear Test Passed.\n");
}

int main(int argc, char **argv)
{
//	test_rrqr(0);
	test_linear();
}
